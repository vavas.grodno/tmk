
(function($) {
  "use strict"; // Start of use strict

  var btn = $('.scroll-to-top');

  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });

  btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
  });


  $("a.scrollto").click(function() {
    var elementClick = $(this).attr("href")
    var destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 800);
    return false;
  });

  $('.slide-one').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:1,
    autoplay:true,
    autoHeight:true,
    autoplayTimeout: 4000,
    autoplaySpeed: 3500,
})


$(".slide-two").owlCarousel({
  loop:true, 
  margin:22, 
  nav:true,
  dots:true,
  dotData:true, 
  autoplay:true, 
  smartSpeed: 2000, 
  autoplayTimeout: 4000,
  responsive:{ 
      0:{
          items:1
      },
      768:{
          items:2
      }
  }
});





})(jQuery); 

$(function() {
  function init() {
    $('[data-behaviour="toggle-menu-icon"]').on('click', toggleMenuIcon);
    $('[data-behaviour="toggle-link-icon"]').on('click', toggleSubMenu);
  };
  
  function toggleMenuIcon() {
    $(this).toggleClass('menu-icon--open');
    $('[data-element="toggle-nav"]').toggleClass('nav--active');
  };
  
  function toggleSubMenu() {
    $(this).toggleClass('nav__link--plus nav__link--minus');
    $('[data-behaviour="toggle-sub-menu"]').slideToggle('nav__sub-list--active');
  };
  
  init()
});


/* Открыть боковую навигацию */
function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
}

/* закрыть/скрыть боковую навигацию */
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

